package com.javarush.task.task14.task1420;

/* 
НОД
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            int x = Integer.parseInt(reader.readLine());
            int y = Integer.parseInt(reader.readLine());
            x = Math.abs(x);
            y = Math.abs(y);

            int result = 0;

            for (int i = x; i > 0; i--) {
                if (x % i == 0 && y % i == 0) {
                    result = i;
                    break;
                }
            }
            System.out.println(result);

        }catch(Exception e) {
            System.out.println("Something went wrong. Did you enter valid numbers?");
        }
    }
}
